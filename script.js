const getData = () => {
fetch('https://jsonplaceholder.typicode.com/users/').then((response) => {
	return response.json();
}).then((data) => {
	console.log(data)
	let posts = "";
	data.forEach((post) => {
		`<div class="row row-cols-1 row-cols-md-3"></div>`
		posts += 
 		`<div class="col mb-4">
 		<div class="card">
		<img src="img.png" class="card-img-top" width="50px">
		<div class="card-body">
		<h5 class="card-title">${post.name}</h5>
		<p class="card-text">${post.email}</p>
		<p class="card-text">${post.company.name}</p>
		<p class="card-text">${post.phone}</p>
		</div>
		</div>
		</div>`;
	});
document.querySelector('#output').innerHTML = posts;

}).catch((err) => {
	console.log('rejected');
});
};

//Add Post

const addPost = (e) => {
	e.preventDefault();
	let title = document.querySelector('#title').value;
	let body1 = document.querySelector('#body1').value;
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		headers: {
			'Accept': 'application/json , text/plain',
			'Content-type': 'application/json'
		},
		body: JSON.stringify({
				name: title,
				email: body1,
				company: body1,
				phone: body1
			})
	}).then(response => response.json()).then(
		data => console.log(data))
}

const btn1 = document.querySelector("#btn1");
btn1.addEventListener('click', getData);
const addForm = document.querySelector('#addPost');
addForm.addEventListener('submit', addPost);